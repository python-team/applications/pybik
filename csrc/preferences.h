/********************************************************************************
** Form generated from reading UI file 'preferences.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DialogPreferences
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab_1;
    QGridLayout *gridLayout;
    QLabel *label;
    QSlider *slider_animspeed;
    QPushButton *button_animspeed_reset;
    QCheckBox *checkbox_mirror_faces;
    QDoubleSpinBox *spinbox_mirror_faces;
    QPushButton *button_mirror_faces_reset;
    QFrame *line;
    QLabel *label_6;
    QComboBox *combobox_shader;
    QPushButton *button_shader_reset;
    QLabel *label_2;
    QComboBox *combobox_samples;
    QPushButton *button_antialiasing_reset;
    QLabel *label_needs_restarted;
    QSpacerItem *verticalSpacer;
    QWidget *tab_2;
    QGridLayout *gridLayout_2;
    QRadioButton *button_mousemode_quad;
    QRadioButton *button_mousemode_ext;
    QRadioButton *button_mousemode_gesture;
    QSpacerItem *verticalSpacer2;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_2;
    QTreeView *listview_movekeys;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QToolButton *button_movekey_add;
    QToolButton *button_movekey_remove;
    QToolButton *button_movekey_reset;
    QWidget *tab_4;
    QGridLayout *gridLayout_3;
    QListView *listview_faces;
    QLabel *label_5;
    QPushButton *button_color;
    QPushButton *button_color_reset;
    QLabel *label_7;
    QComboBox *combobox_image;
    QPushButton *button_image_reset;
    QRadioButton *radiobutton_tiled;
    QRadioButton *radiobutton_mosaic;
    QLabel *label_8;
    QPushButton *button_background_color;
    QPushButton *button_background_color_reset;
    QSpacerItem *verticalSpacer_2;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *DialogPreferences)
    {
        if (DialogPreferences->objectName().isEmpty())
            DialogPreferences->setObjectName(QStringLiteral("DialogPreferences"));
        verticalLayout = new QVBoxLayout(DialogPreferences);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget = new QTabWidget(DialogPreferences);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab_1 = new QWidget();
        tab_1->setObjectName(QStringLiteral("tab_1"));
        gridLayout = new QGridLayout(tab_1);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(tab_1);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        slider_animspeed = new QSlider(tab_1);
        slider_animspeed->setObjectName(QStringLiteral("slider_animspeed"));
        slider_animspeed->setMinimum(1);
        slider_animspeed->setMaximum(100);
        slider_animspeed->setOrientation(Qt::Horizontal);
        slider_animspeed->setTickPosition(QSlider::TicksBelow);
        slider_animspeed->setTickInterval(0);

        gridLayout->addWidget(slider_animspeed, 1, 0, 1, 2);

        button_animspeed_reset = new QPushButton(tab_1);
        button_animspeed_reset->setObjectName(QStringLiteral("button_animspeed_reset"));

        gridLayout->addWidget(button_animspeed_reset, 1, 2, 1, 1);

        checkbox_mirror_faces = new QCheckBox(tab_1);
        checkbox_mirror_faces->setObjectName(QStringLiteral("checkbox_mirror_faces"));
        checkbox_mirror_faces->setChecked(false);

        gridLayout->addWidget(checkbox_mirror_faces, 2, 0, 1, 1);

        spinbox_mirror_faces = new QDoubleSpinBox(tab_1);
        spinbox_mirror_faces->setObjectName(QStringLiteral("spinbox_mirror_faces"));
        spinbox_mirror_faces->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(spinbox_mirror_faces->sizePolicy().hasHeightForWidth());
        spinbox_mirror_faces->setSizePolicy(sizePolicy);
        spinbox_mirror_faces->setSingleStep(0.1);

        gridLayout->addWidget(spinbox_mirror_faces, 2, 1, 1, 1);

        button_mirror_faces_reset = new QPushButton(tab_1);
        button_mirror_faces_reset->setObjectName(QStringLiteral("button_mirror_faces_reset"));

        gridLayout->addWidget(button_mirror_faces_reset, 2, 2, 1, 1);

        line = new QFrame(tab_1);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line, 3, 0, 1, 3);

        label_6 = new QLabel(tab_1);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 4, 0, 1, 1);

        combobox_shader = new QComboBox(tab_1);
        combobox_shader->setObjectName(QStringLiteral("combobox_shader"));

        gridLayout->addWidget(combobox_shader, 4, 1, 1, 1);

        button_shader_reset = new QPushButton(tab_1);
        button_shader_reset->setObjectName(QStringLiteral("button_shader_reset"));

        gridLayout->addWidget(button_shader_reset, 4, 2, 1, 1);

        label_2 = new QLabel(tab_1);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 5, 0, 1, 1);

        combobox_samples = new QComboBox(tab_1);
        combobox_samples->setObjectName(QStringLiteral("combobox_samples"));

        gridLayout->addWidget(combobox_samples, 5, 1, 1, 1);

        button_antialiasing_reset = new QPushButton(tab_1);
        button_antialiasing_reset->setObjectName(QStringLiteral("button_antialiasing_reset"));

        gridLayout->addWidget(button_antialiasing_reset, 5, 2, 1, 1);

        label_needs_restarted = new QLabel(tab_1);
        label_needs_restarted->setObjectName(QStringLiteral("label_needs_restarted"));
        label_needs_restarted->setTextFormat(Qt::PlainText);
        label_needs_restarted->setWordWrap(true);

        gridLayout->addWidget(label_needs_restarted, 6, 0, 1, 3);

        verticalSpacer = new QSpacerItem(20, 108, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 7, 0, 1, 1);

        tabWidget->addTab(tab_1, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout_2 = new QGridLayout(tab_2);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        button_mousemode_quad = new QRadioButton(tab_2);
        button_mousemode_quad->setObjectName(QStringLiteral("button_mousemode_quad"));

        gridLayout_2->addWidget(button_mousemode_quad, 0, 0, 1, 1);

        button_mousemode_ext = new QRadioButton(tab_2);
        button_mousemode_ext->setObjectName(QStringLiteral("button_mousemode_ext"));

        gridLayout_2->addWidget(button_mousemode_ext, 1, 0, 1, 1);

        button_mousemode_gesture = new QRadioButton(tab_2);
        button_mousemode_gesture->setObjectName(QStringLiteral("button_mousemode_gesture"));

        gridLayout_2->addWidget(button_mousemode_gesture, 2, 0, 1, 1);

        verticalSpacer2 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_2->addItem(verticalSpacer2, 3, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_2 = new QVBoxLayout(tab_3);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        listview_movekeys = new QTreeView(tab_3);
        listview_movekeys->setObjectName(QStringLiteral("listview_movekeys"));
        listview_movekeys->setUniformRowHeights(true);

        verticalLayout_2->addWidget(listview_movekeys);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        button_movekey_add = new QToolButton(tab_3);
        button_movekey_add->setObjectName(QStringLiteral("button_movekey_add"));
        button_movekey_add->setToolButtonStyle(Qt::ToolButtonIconOnly);

        horizontalLayout->addWidget(button_movekey_add);

        button_movekey_remove = new QToolButton(tab_3);
        button_movekey_remove->setObjectName(QStringLiteral("button_movekey_remove"));
        button_movekey_remove->setToolButtonStyle(Qt::ToolButtonIconOnly);

        horizontalLayout->addWidget(button_movekey_remove);

        button_movekey_reset = new QToolButton(tab_3);
        button_movekey_reset->setObjectName(QStringLiteral("button_movekey_reset"));
        button_movekey_reset->setToolButtonStyle(Qt::ToolButtonIconOnly);

        horizontalLayout->addWidget(button_movekey_reset);


        verticalLayout_2->addLayout(horizontalLayout);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        gridLayout_3 = new QGridLayout(tab_4);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        listview_faces = new QListView(tab_4);
        listview_faces->setObjectName(QStringLiteral("listview_faces"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(listview_faces->sizePolicy().hasHeightForWidth());
        listview_faces->setSizePolicy(sizePolicy1);
        listview_faces->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        listview_faces->setEditTriggers(QAbstractItemView::NoEditTriggers);
        listview_faces->setTextElideMode(Qt::ElideNone);
        listview_faces->setUniformItemSizes(true);

        gridLayout_3->addWidget(listview_faces, 0, 0, 5, 1);

        label_5 = new QLabel(tab_4);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_3->addWidget(label_5, 0, 1, 1, 1);

        button_color = new QPushButton(tab_4);
        button_color->setObjectName(QStringLiteral("button_color"));
        sizePolicy.setHeightForWidth(button_color->sizePolicy().hasHeightForWidth());
        button_color->setSizePolicy(sizePolicy);
        button_color->setText(QStringLiteral("Color"));

        gridLayout_3->addWidget(button_color, 0, 2, 1, 1);

        button_color_reset = new QPushButton(tab_4);
        button_color_reset->setObjectName(QStringLiteral("button_color_reset"));

        gridLayout_3->addWidget(button_color_reset, 0, 3, 1, 1);

        label_7 = new QLabel(tab_4);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_3->addWidget(label_7, 1, 1, 1, 1);

        combobox_image = new QComboBox(tab_4);
        combobox_image->setObjectName(QStringLiteral("combobox_image"));
        sizePolicy.setHeightForWidth(combobox_image->sizePolicy().hasHeightForWidth());
        combobox_image->setSizePolicy(sizePolicy);
        combobox_image->setIconSize(QSize(32, 32));

        gridLayout_3->addWidget(combobox_image, 1, 2, 1, 1);

        button_image_reset = new QPushButton(tab_4);
        button_image_reset->setObjectName(QStringLiteral("button_image_reset"));

        gridLayout_3->addWidget(button_image_reset, 1, 3, 1, 1);

        radiobutton_tiled = new QRadioButton(tab_4);
        radiobutton_tiled->setObjectName(QStringLiteral("radiobutton_tiled"));
        sizePolicy.setHeightForWidth(radiobutton_tiled->sizePolicy().hasHeightForWidth());
        radiobutton_tiled->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(radiobutton_tiled, 2, 1, 1, 2);

        radiobutton_mosaic = new QRadioButton(tab_4);
        radiobutton_mosaic->setObjectName(QStringLiteral("radiobutton_mosaic"));
        sizePolicy.setHeightForWidth(radiobutton_mosaic->sizePolicy().hasHeightForWidth());
        radiobutton_mosaic->setSizePolicy(sizePolicy);

        gridLayout_3->addWidget(radiobutton_mosaic, 3, 1, 1, 2);

        label_8 = new QLabel(tab_4);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_3->addWidget(label_8, 5, 1, 1, 1);

        button_background_color = new QPushButton(tab_4);
        button_background_color->setObjectName(QStringLiteral("button_background_color"));
        sizePolicy.setHeightForWidth(button_background_color->sizePolicy().hasHeightForWidth());
        button_background_color->setSizePolicy(sizePolicy);
        button_background_color->setText(QStringLiteral("Color"));

        gridLayout_3->addWidget(button_background_color, 5, 2, 1, 1);

        button_background_color_reset = new QPushButton(tab_4);
        button_background_color_reset->setObjectName(QStringLiteral("button_background_color_reset"));

        gridLayout_3->addWidget(button_background_color_reset, 5, 3, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_2, 4, 1, 1, 1);

        tabWidget->addTab(tab_4, QString());

        verticalLayout->addWidget(tabWidget);

        buttonBox = new QDialogButtonBox(DialogPreferences);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(DialogPreferences);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogPreferences, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogPreferences, SLOT(reject()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(DialogPreferences);
    } // setupUi

    void retranslateUi(QDialog *DialogPreferences)
    {
        DialogPreferences->setWindowTitle(gettext_translate("Preferences", Q_NULLPTR));
        label->setText(gettext_translate("Animation Speed:", Q_NULLPTR));
        button_animspeed_reset->setText(QString());
        checkbox_mirror_faces->setText(gettext_translate("Mirror Distance:", Q_NULLPTR));
        button_mirror_faces_reset->setText(QString());
        label_6->setText(gettext_translate("Quality:", Q_NULLPTR));
        button_shader_reset->setText(QString());
#ifndef QT_NO_TOOLTIP
        label_2->setToolTip(gettext_translate("Lower antialiasing has better performance, higher antialiasing has better quality.", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        label_2->setText(gettext_translate("Antialiasing:", Q_NULLPTR));
        button_antialiasing_reset->setText(QString());
        label_needs_restarted->setText(gettext_translate("The program needs to be restarted for the changes to take effect.", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_1), gettext_translate("Graphic", Q_NULLPTR));
        button_mousemode_quad->setText(gettext_translate("Point and click, all directions", Q_NULLPTR));
        button_mousemode_ext->setText(gettext_translate("Point and click, simplified", Q_NULLPTR));
        button_mousemode_gesture->setText(gettext_translate("Gestures", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), gettext_translate("Mouse", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        button_movekey_add->setToolTip(gettext_translate("Add", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        button_movekey_add->setText(gettext_translate("Add", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        button_movekey_remove->setToolTip(gettext_translate("Remove", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        button_movekey_remove->setText(gettext_translate("Remove", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        button_movekey_reset->setToolTip(gettext_translate("Reset", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        button_movekey_reset->setText(gettext_translate("Reset", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), gettext_translate("Keys", Q_NULLPTR));
        label_5->setText(gettext_translate("Color:", Q_NULLPTR));
        button_color_reset->setText(QString());
        label_7->setText(gettext_translate("Image File:", Q_NULLPTR));
        button_image_reset->setText(QString());
        radiobutton_tiled->setText(gettext_translate("Tiled", Q_NULLPTR));
        radiobutton_mosaic->setText(gettext_translate("Mosaic", Q_NULLPTR));
        label_8->setText(gettext_translate("Background:", Q_NULLPTR));
        button_background_color_reset->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_4), gettext_translate("Appearance", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DialogPreferences: public Ui_DialogPreferences {};
} // namespace Ui

QT_END_NAMESPACE

#endif // PREFERENCES_H

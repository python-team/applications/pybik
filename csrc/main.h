/********************************************************************************
** Form generated from reading UI file 'main.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MAIN_H
#define MAIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "../../../pybiklib/ext/qtui_p.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_challenge;
    QAction *action_new_solved;
    QAction *action_quit;
    QAction *action_selectmodel;
    QAction *action_selectmodel_back;
    QAction *action_initial_state;
    QAction *action_reset_rotation;
    QAction *action_preferences;
    QAction *action_statusbar;
    QAction *action_info;
    QAction *action_rewind;
    QAction *action_previous;
    QAction *action_stop;
    QAction *action_play;
    QAction *action_next;
    QAction *action_forward;
    QAction *action_mark_set;
    QAction *action_mark_remove;
    QAction *action_editbar;
    QAction *action_help;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_3;
    QStackedWidget *stackedwidget;
    QWidget *page_game;
    QVBoxLayout *verticalLayout_2;
    QFrame *frame_editbar;
    QHBoxLayout *layout_moves;
    QToolButton *button_edit_exec;
    QToolButton *button_edit_clear;
    QSplitter *splitter;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_debug_text;
    QWidget *box_sidepane;
    QVBoxLayout *layout_sidepane;
    QWidget *page_modelselection;
    QVBoxLayout *verticalLayout_9;
    ModelSelectionWidget *listwidget;
    QStatusBar *statusbar;
    QToolBar *toolbar_play;
    QToolBar *toolbar_selectmodel;
    QToolBar *toolbar_common;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        action_challenge = new QAction(MainWindow);
        action_challenge->setObjectName(QStringLiteral("action_challenge"));
        action_challenge->setShortcut(QStringLiteral("Ctrl+N"));
        action_new_solved = new QAction(MainWindow);
        action_new_solved->setObjectName(QStringLiteral("action_new_solved"));
        action_new_solved->setShortcut(QStringLiteral("Ctrl+Shift+N"));
        action_quit = new QAction(MainWindow);
        action_quit->setObjectName(QStringLiteral("action_quit"));
        action_quit->setShortcut(QStringLiteral("Ctrl+Q"));
        action_selectmodel = new QAction(MainWindow);
        action_selectmodel->setObjectName(QStringLiteral("action_selectmodel"));
        action_selectmodel_back = new QAction(MainWindow);
        action_selectmodel_back->setObjectName(QStringLiteral("action_selectmodel_back"));
        action_selectmodel_back->setShortcut(QStringLiteral("Esc"));
        action_initial_state = new QAction(MainWindow);
        action_initial_state->setObjectName(QStringLiteral("action_initial_state"));
        action_reset_rotation = new QAction(MainWindow);
        action_reset_rotation->setObjectName(QStringLiteral("action_reset_rotation"));
        action_preferences = new QAction(MainWindow);
        action_preferences->setObjectName(QStringLiteral("action_preferences"));
        action_statusbar = new QAction(MainWindow);
        action_statusbar->setObjectName(QStringLiteral("action_statusbar"));
        action_statusbar->setCheckable(true);
        action_statusbar->setChecked(true);
        action_info = new QAction(MainWindow);
        action_info->setObjectName(QStringLiteral("action_info"));
        action_rewind = new QAction(MainWindow);
        action_rewind->setObjectName(QStringLiteral("action_rewind"));
        action_previous = new QAction(MainWindow);
        action_previous->setObjectName(QStringLiteral("action_previous"));
        action_stop = new QAction(MainWindow);
        action_stop->setObjectName(QStringLiteral("action_stop"));
        action_play = new QAction(MainWindow);
        action_play->setObjectName(QStringLiteral("action_play"));
        action_next = new QAction(MainWindow);
        action_next->setObjectName(QStringLiteral("action_next"));
        action_forward = new QAction(MainWindow);
        action_forward->setObjectName(QStringLiteral("action_forward"));
        action_mark_set = new QAction(MainWindow);
        action_mark_set->setObjectName(QStringLiteral("action_mark_set"));
        action_mark_remove = new QAction(MainWindow);
        action_mark_remove->setObjectName(QStringLiteral("action_mark_remove"));
        action_editbar = new QAction(MainWindow);
        action_editbar->setObjectName(QStringLiteral("action_editbar"));
        action_editbar->setCheckable(true);
        action_editbar->setChecked(true);
        action_help = new QAction(MainWindow);
        action_help->setObjectName(QStringLiteral("action_help"));
#ifndef QT_NO_TOOLTIP
        action_help->setToolTip(QStringLiteral("Help"));
#endif // QT_NO_TOOLTIP
        action_help->setShortcut(QStringLiteral("F1"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        verticalLayout_3 = new QVBoxLayout(centralwidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        stackedwidget = new QStackedWidget(centralwidget);
        stackedwidget->setObjectName(QStringLiteral("stackedwidget"));
        page_game = new QWidget();
        page_game->setObjectName(QStringLiteral("page_game"));
        verticalLayout_2 = new QVBoxLayout(page_game);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        frame_editbar = new QFrame(page_game);
        frame_editbar->setObjectName(QStringLiteral("frame_editbar"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame_editbar->sizePolicy().hasHeightForWidth());
        frame_editbar->setSizePolicy(sizePolicy);
        frame_editbar->setLayoutDirection(Qt::LeftToRight);
        frame_editbar->setFrameShape(QFrame::StyledPanel);
        frame_editbar->setFrameShadow(QFrame::Raised);
        frame_editbar->setLineWidth(0);
        frame_editbar->setMidLineWidth(0);
        layout_moves = new QHBoxLayout(frame_editbar);
        layout_moves->setSpacing(0);
        layout_moves->setObjectName(QStringLiteral("layout_moves"));
        layout_moves->setContentsMargins(0, 0, 0, 0);
        button_edit_exec = new QToolButton(frame_editbar);
        button_edit_exec->setObjectName(QStringLiteral("button_edit_exec"));
        button_edit_exec->setFocusPolicy(Qt::NoFocus);
        button_edit_exec->setAutoRaise(true);

        layout_moves->addWidget(button_edit_exec);

        button_edit_clear = new QToolButton(frame_editbar);
        button_edit_clear->setObjectName(QStringLiteral("button_edit_clear"));
        button_edit_clear->setFocusPolicy(Qt::NoFocus);
        button_edit_clear->setAutoRaise(true);

        layout_moves->addWidget(button_edit_clear);


        verticalLayout_2->addWidget(frame_editbar);

        splitter = new QSplitter(page_game);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_debug_text = new QLabel(layoutWidget);
        label_debug_text->setObjectName(QStringLiteral("label_debug_text"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_debug_text->sizePolicy().hasHeightForWidth());
        label_debug_text->setSizePolicy(sizePolicy1);
        label_debug_text->setText(QStringLiteral(""));
        label_debug_text->setTextFormat(Qt::PlainText);

        verticalLayout->addWidget(label_debug_text);

        splitter->addWidget(layoutWidget);
        box_sidepane = new QWidget(splitter);
        box_sidepane->setObjectName(QStringLiteral("box_sidepane"));
        layout_sidepane = new QVBoxLayout(box_sidepane);
        layout_sidepane->setSpacing(0);
        layout_sidepane->setObjectName(QStringLiteral("layout_sidepane"));
        layout_sidepane->setContentsMargins(0, 0, 0, 0);
        splitter->addWidget(box_sidepane);

        verticalLayout_2->addWidget(splitter);

        stackedwidget->addWidget(page_game);
        page_modelselection = new QWidget();
        page_modelselection->setObjectName(QStringLiteral("page_modelselection"));
        verticalLayout_9 = new QVBoxLayout(page_modelselection);
        verticalLayout_9->setSpacing(0);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        verticalLayout_9->setContentsMargins(0, 0, 0, 0);
        listwidget = new ModelSelectionWidget(page_modelselection);
        listwidget->setObjectName(QStringLiteral("listwidget"));
        listwidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        listwidget->setDragDropMode(QAbstractItemView::NoDragDrop);
        listwidget->setIconSize(QSize(128, 128));
        listwidget->setMovement(QListView::Static);
        listwidget->setViewMode(QListView::IconMode);
        listwidget->setUniformItemSizes(true);
        listwidget->setWordWrap(true);

        verticalLayout_9->addWidget(listwidget);

        stackedwidget->addWidget(page_modelselection);

        verticalLayout_3->addWidget(stackedwidget);

        MainWindow->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);
        toolbar_play = new QToolBar(MainWindow);
        toolbar_play->setObjectName(QStringLiteral("toolbar_play"));
        toolbar_play->setMovable(false);
        toolbar_play->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolbar_play);
        toolbar_selectmodel = new QToolBar(MainWindow);
        toolbar_selectmodel->setObjectName(QStringLiteral("toolbar_selectmodel"));
        toolbar_selectmodel->setMovable(false);
        toolbar_selectmodel->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolbar_selectmodel);
        toolbar_common = new QToolBar(MainWindow);
        toolbar_common->setObjectName(QStringLiteral("toolbar_common"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(toolbar_common->sizePolicy().hasHeightForWidth());
        toolbar_common->setSizePolicy(sizePolicy2);
        toolbar_common->setMovable(false);
        toolbar_common->setFloatable(false);
        MainWindow->addToolBar(Qt::TopToolBarArea, toolbar_common);

        retranslateUi(MainWindow);

        stackedwidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(gettext_translate("Pybik", Q_NULLPTR));
        action_challenge->setText(gettext_translate("&New Challenge", Q_NULLPTR));
        action_new_solved->setText(gettext_translate("Ne&w Solved", Q_NULLPTR));
        action_quit->setText(gettext_translate("&Quit", Q_NULLPTR));
        action_selectmodel->setText(gettext_translate("Select Puzzle", Q_NULLPTR));
        action_initial_state->setText(gettext_translate("&Set as Initial State", Q_NULLPTR));
        action_reset_rotation->setText(gettext_translate("&Reset Rotation", Q_NULLPTR));
        action_preferences->setText(gettext_translate("&Preferences \342\200\246", Q_NULLPTR));
        action_statusbar->setText(gettext_translate("&Status Bar", Q_NULLPTR));
        action_info->setText(gettext_translate("&Info \342\200\246", Q_NULLPTR));
        action_rewind->setText(gettext_translate("Rewind", Q_NULLPTR));
        action_previous->setText(gettext_translate("Previous", Q_NULLPTR));
        action_stop->setText(gettext_translate("Stop", Q_NULLPTR));
        action_play->setText(gettext_translate("Play", Q_NULLPTR));
        action_next->setText(gettext_translate("Next", Q_NULLPTR));
        action_forward->setText(gettext_translate("Forward", Q_NULLPTR));
        action_mark_set->setText(gettext_translate("Add Mark", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        action_mark_set->setToolTip(gettext_translate("Mark the current place in the sequence of moves", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        action_mark_remove->setText(gettext_translate("Remove Mark", Q_NULLPTR));
#ifndef QT_NO_TOOLTIP
        action_mark_remove->setToolTip(gettext_translate("Remove the mark at the current place in the sequence of moves", Q_NULLPTR));
#endif // QT_NO_TOOLTIP
        action_editbar->setText(gettext_translate("&Edit Bar", Q_NULLPTR));
        action_help->setText(gettext_translate("&Help \342\200\246", Q_NULLPTR));
        action_help->setIconText(gettext_translate("Help", Q_NULLPTR));
        button_edit_exec->setText(QString());
        button_edit_clear->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MAIN_H

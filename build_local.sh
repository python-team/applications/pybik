#!/bin/sh

set -e
./setup.py build --inplace --parallel=True "$@" build_doc install_desktop --ask

#version 120
//  Copyright © 2013, 2015, 2017  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

attribute vec4 vertex_attr;
attribute vec3 normal_attr;
varying vec3 normal;
uniform mat4 projection;
uniform mat4 modelview;
uniform mat4 object;

void main()
{
    gl_Position = projection * (modelview * (object * vertex_attr));
    // actually we need here the inverse transpose of the matrix (modelview*object),
    // but the mat3 parts are pure rotations, where the inverse is equal to the transpose
    normal = mat3(modelview) * (mat3(object) * normal_attr);
}


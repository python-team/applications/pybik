#version 120
//  Copyright © 2014-2017  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

attribute vec4 vertex_attr;
attribute vec3 color_attr;
attribute vec3 barycentric_attr;

varying vec4 color;

uniform mat4 projection;
uniform mat4 modelview;
uniform mat4 object;

void main()
{
    gl_Position = projection * (modelview * (object * vertex_attr));
    color.rgb = color_attr;
    color.a = (barycentric_attr == vec3(0.)) ? 0. : 1.;
}


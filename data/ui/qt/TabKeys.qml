//  Copyright © 2015-2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

    ColumnLayout {
        id: verticalLayout_2
        
        Component {
            id: editableDelegate
            Item {
                Text {
                    width: parent.width
                    anchors.margins: 4
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    visible: !styleData.selected || styleData.role == "key"
                    elide: styleData.elideMode
                    color: styleData.textColor
                    text: styleData.value
                    property bool editing: false
                    MouseArea {
                        anchors.fill: parent
                        enabled: styleData.selected && !parent.editing
                        onClicked: {
                                parent.forceActiveFocus()
                                parent.editing = true
                                parent.text = ctx.tr("Press a key …")
                        }
                    }
                    onActiveFocusChanged: {
                        if (editing && !activeFocus) {
                            text = styleData.value
                            editing = false
                    }   }
                    Keys.onPressed: {
                        if (editing) {
                            if (event.key == Qt.Key_Escape) {
                                text = styleData.value
                                event.accepted = true
                                editing = false
                                focus = false
                            }
                            else if (event.count == 1) {
                                var keystring = ctx.keytostring(event.key, event.modifiers)
                                if (keystring != '') {
                                    ctx.movekey_model[styleData.row].key = keystring
                                    ctx.movekey_item_changed()
                                    text = styleData.value
                                    event.accepted = true
                                    editing = false
                                    focus = false
                    }   }   }   }
                }
                Rectangle {
                    anchors.fill: parent
                    visible: styleData.selected && styleData.role == "text"
                    color: "transparent"
                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 1
                        color: textinput.activeFocus ? palette.base : "transparent"
                        TextInput {
                            id: textinput
                            width: parent.width
                            anchors.margins: 3
                            anchors.left: parent.left
                            anchors.verticalCenter: parent.verticalCenter
                            color: activeFocus ? palette.text : styleData.textColor
                            text: styleData.value
                            
                            property bool editing: false
                            onActiveFocusChanged: { if (activeFocus)  editing = true }
                            onEditingFinished: {
                                if (editing) {
                                    ctx.movekey_model[styleData.row].text = text
                                    ctx.movekey_item_changed()
                                    editing = false
                                    focus = false
                            }   }
                            Keys.onPressed: {
                                if (event.key == Qt.Key_Escape) {
                                    text = styleData.value
                                    event.accepted = true
                                    editing = false
                                    focus = false
                            }   }
        }   }   }   }   }
        TableView {
            id: listview_movekeys
            Layout.fillHeight: true
            Layout.minimumHeight: 200
            Layout.fillWidth: true
            Layout.minimumWidth: 100
            alternatingRowColors: false
            model: ctx.movekey_model
            itemDelegate: editableDelegate
            
            TableViewColumn {
                role: "text"
                title: ctx.tr("Move")
            }
            TableViewColumn {
                role: "key"
                title: ctx.tr("Key")
            }
        }
        
        RowLayout {
            id: horizontalLayout
            Rectangle {
                id: horizontalSpacer
                Layout.fillWidth: true
                implicitWidth: 40
                implicitHeight: 20
            }
            ToolButton {
                id: button_movekey_add
                tooltip: ctx.tr("Add")
                iconName: "list-add"
                onClicked: {
                    listview_movekeys.currentRow = ctx.movekey_item_added(listview_movekeys.currentRow)
                    listview_movekeys.selection.select(listview_movekeys.currentRow)
                    //self.ui.listview_movekeys.edit(index)
                }
            }
            ToolButton {
                id: button_movekey_remove
                tooltip: ctx.tr("Remove")
                iconName: "list-remove"
                onClicked: {
                    if (listview_movekeys.currentRow >= 0) {
                        listview_movekeys.currentRow = ctx.movekey_item_removed(listview_movekeys.currentRow)
                        listview_movekeys.selection.select(listview_movekeys.currentRow)
                }   }
            }
            ToolButton {
                id: button_movekey_reset
                tooltip: ctx.tr("Reset")
                iconName: "document-revert"
                onClicked: ctx.movekey_items_reset()
            }
        }
    }


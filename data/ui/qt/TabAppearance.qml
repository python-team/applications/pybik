//  Copyright © 2015-2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

    GridLayout {
        columns: 4
        
        Component.onCompleted: {
            listview_faces.selection.select(listview_faces.currentRow)
        }
        
        Connections {
            target: ctx
            onPrefupdateface: listview_faces.modelitem = ctx.facesmodelitem(listview_faces.currentkey)
        }
        
        TableView {
            id: listview_faces
            Layout.row: 0; Layout.column: 0; Layout.rowSpan: 4
            Layout.fillHeight: true
            implicitWidth: 120 //TODO: width of content
            model: ctx.facesmodel
            property var currentkey
            property var modelitem
            onCurrentRowChanged: {
                currentkey = model[currentRow].key
                modelitem = ctx.facesmodelitem(currentkey)
            }
            alternatingRowColors: false
            headerVisible: false
            currentRow: 0
            
            TableViewColumn { role: "text" }
        }
        Label {
            Layout.row: 0; Layout.column: 1
            text: ctx.tr("Color:")
        }
        ColorButton {
            id: button_color
            Layout.row: 0; Layout.column: 2
            Binding on color { value: listview_faces.modelitem.color }
            onColorChanged: ctx.set_settings_arg("theme.faces.{}.color", listview_faces.currentkey, color.toString())
        }
        Button {
            id: button_color_reset
            Layout.row: 0; Layout.column: 3
            iconName: "edit-clear"
            onClicked: {
                ctx.del_settings_arg("theme.faces.{}.color", listview_faces.currentkey)
                ctx.prefupdateface()
            }
        }
        Label {
            Layout.row: 1; Layout.column: 1
            text: ctx.tr("Image File:")
        }
        ImageSelector {
                id: imageselector
                Layout.row: 1; Layout.column: 2
                z: 1
            
                selectedvalue: listview_faces.modelitem.image
                currentfolder: listview_faces.modelitem.folder
                onImage_selected: {
                    ctx.set_settings_arg("theme.faces.{}.image", listview_faces.currentkey, name)
                    ctx.prefupdateface()
                }
                onImagefile_selected: {
                    ctx.set_settings_arg_url("theme.faces.{}.image", listview_faces.currentkey, url)
                    ctx.prefupdateface()
                }
        }
        Button {
            id: button_image_reset
            Layout.row: 1; Layout.column: 3
            iconName: "edit-clear"
            onClicked: {
                ctx.del_settings_arg("theme.faces.{}.image", listview_faces.currentkey)
                ctx.del_settings_arg("theme.faces.{}.mode", listview_faces.currentkey)
                ctx.prefupdateface()
            }
        }
        ExclusiveGroup { id: imagemodeGroup }
        RadioButton {
            id: radiobutton_tiled
            Layout.row: 2; Layout.column: 1; Layout.columnSpan: 3
            exclusiveGroup: imagemodeGroup
            text: ctx.tr("Tiled")
            Binding on checked { value: listview_faces.modelitem.imagemode == "tiled" }
            onCheckedChanged: {
                if (checked)  ctx.set_settings_arg("theme.faces.{}.mode_nick", listview_faces.currentkey, "tiled")
            }
        }
        RadioButton {
            id: radiobutton_mosaic
            Layout.row: 3; Layout.column: 1; Layout.columnSpan: 3
            exclusiveGroup: imagemodeGroup
            text: ctx.tr("Mosaic")
            Binding on checked { value: listview_faces.modelitem.imagemode == "mosaic" }
            onCheckedChanged: {
                if (checked)  ctx.set_settings_arg("theme.faces.{}.mode_nick", listview_faces.currentkey, "mosaic")
            }
        }
        Label {
            Layout.row: 4; Layout.column: 1
            text: ctx.tr("Background:")
        }
        ColorButton {
            id: button_bgcolor
            Layout.row: 4; Layout.column: 2
            color: ctx.get_settings("theme.bgcolor")
            onColorChanged: ctx.set_settings("theme.bgcolor", color.toString())
        }
        Button {
            id: button_bgcolor_reset
            Layout.row: 4; Layout.column: 3
            iconName: "edit-clear"
            onClicked: { button_bgcolor.color = ctx.get_settings("theme.bgcolor_default"); ctx.del_settings("theme.bgcolor")}
        }
    }


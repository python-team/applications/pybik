//  Copyright © 2015-2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

GridLayout {
    id: root
    property var mirrorfaces_range: ctx.get_settings("draw.mirror_distance_range")
        
        anchors.fill: parent
        columns: 4
        
        Label {
            Layout.columnSpan: 4
            text: ctx.tr("Animation Speed:")
            Layout.fillWidth: true
            Layout.minimumWidth: 100//implicitWidth
            //Layout.minimumHeight: implicitHeight
        }
        Slider {
            id: slider_animspeed
            Layout.columnSpan: 3
            Layout.fillWidth: true
            Layout.minimumWidth: 50
            //Layout.preferredWidth: 100
            //Layout.maximumWidth: 300
            //Layout.minimumHeight: implicitHeight
            orientation: Qt.Horizontal
            //tickmarksEnabled: true
            //stepSize: 10.0
            onValueChanged: ctx.set_settings("draw.speed", value)
            Component.onCompleted: {
                // this values need to be set in correct order, so that
                // no spurious valueChanged signal emitted. Changing
                // minimumValue/maximumValue will emit such a signal if value
                // is not in the new range.
                var range = ctx.get_settings("draw.speed_range")
                maximumValue = range[1] // initial 1.0
                value = ctx.get_settings("draw.speed") // initial 0.0
                minimumValue = range[0] // initial 0.0
            }
        }
        Button {
            id: button_animspeed_reset
            //Layout.minimumWidth: implicitWidth
            //Layout.minimumHeight: implicitHeight
            iconName: "edit-clear"
            onClicked: { slider_animspeed.value = ctx.get_settings("draw.speed_default"); ctx.del_settings("draw.speed") }
        }
        CheckBox {
            id: checkbox_mirrorfaces
            Layout.columnSpan: 2
            //Layout.minimumWidth: implicitWidth
            //Layout.minimumHeight: implicitHeight
            text: ctx.tr("Mirror Distance:")
            checked: ctx.get_settings("draw.mirror_faces")
            onCheckedChanged: ctx.set_settings("draw.mirror_faces", checked)
        }
        SpinBox {
            id: spinbox_mirrorfaces
            Layout.fillWidth: true
            Layout.minimumWidth: 10//implicitWidth
            enabled: checkbox_mirrorfaces.checked
            //Layout.preferredWidth: implicitWidth
            //Layout.minimumHeight: implicitHeight
            decimals: 1
            stepSize: 0.1
            //implicitWidth: 400
            //width: 400
            minimumValue: root.mirrorfaces_range[0]
            maximumValue: root.mirrorfaces_range[1]
            value: ctx.get_settings("draw.mirror_distance")
            onValueChanged: ctx.set_settings("draw.mirror_distance", value)
        }
        Button {
            id: button_mirror_faces_reset
            //Layout.minimumWidth: implicitWidth
            //Layout.minimumHeight: implicitHeight
            iconName: "edit-clear"
            onClicked: {
                checkbox_mirrorfaces.checked = ctx.get_settings("draw.mirror_faces_default"); ctx.del_settings("draw.mirror_faces")
                spinbox_mirrorfaces.value = ctx.get_settings("draw.mirror_distance_default"); ctx.del_settings("draw.mirror_distance")
            }
        }
        Rectangle {
            id: line
            Layout.columnSpan: 4
            Layout.fillWidth: true
            Layout.minimumWidth: 100//implicitWidth
            //Layout.minimumHeight: implicitHeight
            height: 1
            color: palette.shadow
        }
        Label {
            id: label_6
            //Layout.minimumWidth: implicitWidth
            //Layout.minimumHeight: implicitHeight
            text: ctx.tr("Quality:")
        }
        SimpleList {
            id: list_shader
            Layout.columnSpan: 2
            Layout.fillWidth: true
            Layout.minimumWidth: 10//implicitWidth
            //Layout.minimumHeight: implicitHeight
            model: ctx.shadermodel
            onCurrentIndexChanged: ctx.set_settings("draw.shader", currentIndex)
            onModelChanged: currentIndex = ctx.get_settings("draw.shader")
        }
        Button {
            id: button_shader_reset
            //Layout.minimumWidth: implicitWidth
            //Layout.minimumHeight: implicitHeight
            iconName: "edit-clear"
            onClicked: { list_shader.currentIndex = ctx.get_settings("draw.shader_default"); ctx.del_settings("draw.shader") }
        }
        Label {
            id: label_2
            //Layout.minimumWidth: implicitWidth
            //Layout.minimumHeight: implicitHeight
            text: ctx.tr("Antialiasing:")
            //toolTip: ctx.tr("Lower antialiasing has better performance, higher antialiasing has better quality.")
        }
        SimpleList {
            id: list_samples
            Layout.columnSpan: 2
            Layout.fillWidth: true
            Layout.minimumWidth: 10//implicitWidth
            //Layout.minimumHeight: implicitHeight
            model: ctx.samplemodel
            onCurrentIndexChanged: ctx.set_settings("draw.samples", currentIndex)
            onModelChanged: currentIndex = ctx.sampleindex
        }
        Button {
            id: button_antialiasing_reset
            //Layout.minimumWidth: implicitWidth
            //Layout.minimumHeight: implicitHeight
            iconName: "edit-clear"
            onClicked: { list_samples.currentIndex = ctx.get_settings("draw.samples_default"); ctx.del_settings("draw.samples") }
        }
        Label {
            id: label_needs_restarted
            Layout.columnSpan: 4
            Layout.fillWidth: true
            //Layout.fillHeight: true
            Layout.minimumWidth: 100
            Layout.preferredWidth: 100
            //Layout.minimumHeight: contentHeight//implicitHeight
            //Layout.preferredHeight: contentHeight
            //visible: false
            visible: (ctx.activesamples > 1) && (ctx.activesamples != (1<<list_samples.currentIndex))
            text: ctx.tr('The program needs to be restarted for the changes to take effect.')
            textFormat: Text.PlainText
            color: "#aa0000"
            wrapMode: Text.WordWrap
        }
        //Rectangle {
        //    Layout.columnSpan: 4
        //    Layout.fillWidth: true
        //    Layout.fillHeight: true
        //    Layout.minimumWidth: 20
        //    Layout.minimumHeight: 10
        //    //Layout.preferredHeight: 108
        //    color: "green"//"transparent"
        //}
}


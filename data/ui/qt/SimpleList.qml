//  Copyright © 2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3

Rectangle {
    id: root
    
    border.color: palette.shadow
    border.width: 1
    radius: 3
    color: "transparent"
    implicitHeight: combobox_shader.implicitHeight + 2
    
    property var model
    property int currentIndex: 0
    
    Column {
        id: combobox_shader
        z: -1
        anchors.margins: 1
        anchors.fill: parent
        Repeater {
            id: repeater_shader
            model: root.model
            
            Rectangle {
                width: combobox_shader.width
                height: label_shader.height+6
                border.color: palette.shadow
                border.width: root.currentIndex==index ? 1 : 0
                color: root.currentIndex==index ? palette.highlight : palette.button
                Label {
                    id: label_shader
                    x: 3
                    y: 3
                    height: implicitHeight
                    text: modelData.text
                    color: root.currentIndex==index ? palette.highlightedText : palette.buttonText
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: root.currentIndex = index
                }
            }
        }
    }
}


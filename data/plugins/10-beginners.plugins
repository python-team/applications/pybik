Format: Pybik Plugins Collection - Version 2.0
Copyright: 2015, 2017  B. Clausius <barcc@gmx.de>
License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
Model: Cube 3
Ref-Blocks: f l u b r d

################################
# Solution: pos=block …, moves
#  pos: position on the cube, e.g. "rf" for the front-right edge or "ufr" for
#     the uppper-front-right corner. The order does not matter, "ufr" is equal to "fur".
#  block: The block at the position. The order matters and depends on pos.
#     "fr|fl": "fr" or "fl" is allowed at the position
#     "!fru": every block except "fru" is allowed at the position
#     "*fru": a block in any rotation state of "fru" is allowed at the position (fru, ruf, ufr)
#     "!*fru": every block except all "fru" rotations is allowed at the position
#     "f?u": "?" can be every face
#  moves: The moves that should be applied to the cube if the conditions apply.

Path: /Solvers/Beginner's method
Depends: /Solvers/Beginner's method/Bottom edge place

Path: /Solvers/Beginner's method/Top edges
Solution:
        df=df dl=dl db=db dr=dr  dfl=dfl dlb=dlb dbr=dbr,  @@solved
        uf=uf  ur=ur  ub=ub  ul=ul,  @@solved
        #
        fr=*ur,  r
        br=*ur,  r-
        dr=*ur,  rr
        df=*ur,  d
        db=*ur,  d-
        dl=*ur,  dd
        uf=*ur,  ff
        ub=*ur,  bb
        ul=*ur,  ll
        fl=*ur uf=uf, f-df
        fl=*ur, f-d
        bl=*ur ub=ub, bd-b-
        bl=*ur, bd-
        ur=ru, r-uf-u-
        #
        , D

Path: /Solvers/Beginner's method/Top corners
Depends: /Solvers/Beginner's method/Top edges
Solution:
        df=df dl=dl db=db dr=dr  dfl=dfl dlb=dlb dbr=dbr drf=drf,  @@solved
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf,  @@solved
        #
        ufr=ufr,  D
        #
        fld=*ruf,  d
        brd=*ruf,  d-
        lbd=*ruf,  dd
        rfd=*ruf,  r-d-rd
        ufr=*ufr,  r-d-rd
        ufr=*u??,  r-d-r
        #
        , D

Path: /Solvers/Beginner's method/Middle slice
Depends: /Solvers/Beginner's method/Top corners
Solution:
        fr=fr  rb=rb  bl=bl  lf=lf,  @@solved
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf  uf=uf  ur=ur  ub=ub  ul=ul   dr=!dr,     RR
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf  uf=uf  ur=ur  ub=ub  ul=ul   db=!db,     RR
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf  uf=uf  ur=ur  ub=ub  ul=ul   dl=!dl,     RR
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf  uf=uf  ur=ur  ub=ub  ul=ul   df=!df,     RR
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf  uf=uf  ur=ur  ub=ub  ul=ul   dfr=!dfr,   RR
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf  uf=uf  ur=ur  ub=ub  ul=ul   dbr=!dbr,   RR
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf  uf=uf  ur=ur  ub=ub  ul=ul   dbl=!dbl,   RR
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf  uf=uf  ur=ur  ub=ub  ul=ul   dfl=!dfl,   RR
        #
        fu=fr,  uru-r-u-f-uf
        fu=fl,  u-l-ulufu-f-
        #
        ru=fr|fl,  u
        lu=fr|fl,  u-
        bu=fr|fl,  uu
        #
        fr=rf,  uru-r-u-f-uf
        fl=lf,  u-l-ulufu-f-
        #
        fu=*?u  lu=*?u  bu=*?u  ru=*?u  fr=!fr,  uru-r-u-f-uf
        fu=*?u  lu=*?u  bu=*?u  ru=*?u  fl=!fl,  u-l-ulufu-f-
        , D

Path: /Solvers/Beginner's method/Bottom edge orient
Depends: /Solvers/Beginner's method/Middle slice
Solution:
        uf=uf ur=ur ub=ub ul=ul  ufr=ufr urb=urb ubl=ubl ulf=ulf  df=d? dl=d? db=d? dr=d?,  @@solved
        uf=uf ur=ur ub=ub ul=ul  ufr=ufr urb=urb ubl=ubl ulf=ulf,  RR
        #
        uf=u?  ur=u?  ub=u?  ul=u?,  @@solved
        #
        uf=?u  ur=?u  ub=?u  ul=?u,  furu-r-f-
        #
        uf=u?  ur=?u  ub=?u  ul=u?,  U
        uf=?u  ur=u?  ub=u?  ul=?u,  U-
        uf=u?  ur=u?  ub=?u  ul=?u,  UU
        uf=?u  ur=?u  ub=u?  ul=u?,  furu-r-f-
        #
        uf=u?  ur=?u  ub=u?  ul=?u,  U-
        uf=?u  ur=u?  ub=?u  ul=u?,  frur-u-f-

Path: /Solvers/Beginner's method/Bottom corner orient
Depends: /Solvers/Beginner's method/Bottom edge orient
Solution:
        uf=uf ur=ur ub=ub ul=ul  ufr=ufr urb=urb ubl=ubl ulf=ulf  df=d? dl=d? db=d? dr=d?  dfl=d?? dlb=d?? dbr=d?? drf=d??,  @@solved
        uf=uf ur=ur ub=ub ul=ul  ufr=ufr urb=urb ubl=ubl ulf=ulf,  RR
        #
        uf=u? ur=u? ub=u? ul=u?  ufl=u?? ulb=u?? ubr=u?? urf=u??,  @@solved
        # State 1: No Corner Cubes have upper color upside
        ufl=!u?? ulb=!u?? ubr=!u?? urf=??u,  U
        ufl=?u? ulb=!u?? ubr=!u?? urf=!u??,  U-
        ufl=??u ulb=!u?? ubr=!u?? urf=!u??,  rur-uruur-
        # State 2: One Corner Cube has upper color upside
        ufl=!u?? ulb=!u?? ubr=!u?? urf=u??,  U
        ufl=!u?? ulb=u?? ubr=!u?? urf=!u??,  U-
        ufl=!u?? ulb=!u?? ubr=u?? urf=!u??,  UU
        ufl=u?? ulb=!u?? ubr=!u?? urf=!u??,  rur-uruur-
        # State 3: Two Corner Cubes have upper color upside
        urf=?u?,  U
        ulb=?u?,  U-
        ubr=?u?,  UU
        ufl=?u?,  rur-uruur-

Path: /Solvers/Beginner's method/Bottom corner place
Depends: /Solvers/Beginner's method/Bottom corner orient
Solution:
        uf=uf ur=ur ub=ub ul=ul  ufr=ufr urb=urb ubl=ubl ulf=ulf  df=d? dl=d? db=d? dr=d?  dfl=dfl dlb=dlb dbr=dbr drf=drf,  @@solved
        uf=uf ur=ur ub=ub ul=ul  ufr=ufr urb=urb ubl=ubl ulf=ulf,  FF
        #
        ufr=ufr  urb=urb  ubl=ubl  ulf=ulf,  @@solved
        # Two adjacent corners right
        urb=urb  ubl=ubl,  r-fr-bbrf-r-bbrru-
        ubl=ubl  ulf=ulf,  U
        ufr=ufr  urb=urb,  U-
        ufr=ufr  ulf=ulf,  UU
        # Two diagonal corners right
        ufr=ufr  ubl=ubl,  r-fr-bbrf-r-bbrru-
        urb=urb  ulf=ulf,  r-fr-bbrf-r-bbrru-
        #
        , u

Path: /Solvers/Beginner's method/Bottom edge place
Depends: /Solvers/Beginner's method/Bottom corner place
Solution:
        uf=uf ur=ur ub=ub ul=ul  ufr=ufr urb=urb ubl=ubl ulf=ulf  df=df dl=dl db=db dr=dr  dfl=dfl dlb=dlb dbr=dbr drf=drf,  @@solved
        uf=uf ur=ur ub=ub ul=ul  ufr=ufr urb=urb ubl=ubl ulf=ulf,  FF
        #
        uf=uf ur=ur ub=ub ul=ul,  @@solved
        # rotate 3 edges clockwise
        ub=ub,  ffulr-ffl-ruff
        #
        ul=ul,  U
        ur=ur,  U-
        uf=uf,  UU
        , ffulr-ffl-ruff

# generated with: ./maint.py merge-translators

translators = [
  ('Asturian', 'ast', False, [
    ('Xuacu Saturio', 'https://launchpad.net/~xuacusk8'),
    ('ivarela', 'https://launchpad.net/~ivarela'),
  ]),
  ('Bulgarian', 'bg', False, [
    ('Atanas Kovachki', 'https://launchpad.net/~zdar'),
  ]),
  ('Bosnian', 'bs', False, [
    ('Kenan Dervišević', 'https://launchpad.net/~kenan3008'),
  ]),
  ('Czech', 'cs', False, [
    ('Tadeáš Pařík', 'https://launchpad.net/~pariktadeas'),
  ]),
  ('German', 'de', True, [
    ('B. Clausius', 'https://launchpad.net/~barcc'),
  ]),
  ('Greek', 'el', False, [
    ('George Christofis', 'https://launchpad.net/~geochr'),
    ('mara sdr', 'https://launchpad.net/~paren8esis'),
  ]),
  ('English (United Kingdom)', 'en_GB', True, [
    ('Andi Chandler', 'https://launchpad.net/~bing'),
    ('Anthony Harrington', 'https://launchpad.net/~untaintableangel'),
    ('James Tait', 'https://launchpad.net/~jamestait'),
    ('B. Clausius', 'https://launchpad.net/~barcc'),
  ]),
  ('Spanish', 'es', False, [
    ('Adolfo Jayme', 'https://launchpad.net/~fitojb'),
    ('Paco Molinero', 'https://launchpad.net/~franciscomol'),
    ('Dante Díaz', 'https://launchpad.net/~dante'),
    ('Eduardo Alberto Calvo', 'https://launchpad.net/~edu5800'),
    ('José Lou Chang', 'https://launchpad.net/~obake'),
    ('Oscar Fabian Prieto Gonzalez', 'https://launchpad.net/~ofpprieto'),
    ('Aaron Farias', 'https://launchpad.net/~timido'),
  ]),
  ('Finnish', 'fi', False, [
    ('Jiri Grönroos', 'https://launchpad.net/~jiri-gronroos'),
  ]),
  ('French', 'fr', False, [
    ('Jean-Marc', 'https://launchpad.net/~m-balthazar'),
    ('Nicolas Delvaux', 'https://launchpad.net/~malizor'),
    ('Sylvie Gallet', 'https://launchpad.net/~sylvie-gallet'),
    ('Éfrit', 'https://launchpad.net/~efrit'),
    ('Stanislas Michalak', 'https://launchpad.net/~stanislas-michalak'),
    ('Havok Novak', 'https://launchpad.net/~havok-novak-deactivatedaccount'),
    ('Baptiste Fontaine', 'https://launchpad.net/~bfontaine'),
    ('Célestin Taramarcaz', 'https://launchpad.net/~ctaramarcaz'),
    ('lann', 'https://launchpad.net/~lann'),
    ('Aurélien Ribeiro', 'https://launchpad.net/~aurel-koala'),
    ('Pierre Soulat', 'https://launchpad.net/~pierre-soulat'),
  ]),
  ('Galician', 'gl', True, [
    ('Miguel Anxo Bouzada', 'https://launchpad.net/~mbouzada'),
  ]),
  ('Hebrew', 'he', False, [
    ('Yaron', 'https://launchpad.net/~sh-yaron'),
  ]),
  ('Italian', 'it', False, [
    ('Claudio Arseni', 'https://launchpad.net/~claudio.arseni'),
    ('Gianfranco Frisani', 'https://launchpad.net/~gfrisani'),
    ('acc01291', 'https://launchpad.net/~acc01291'),
  ]),
  ('Kabyle', 'kab', False, [
    ('Belkacem Mohammed', 'https://launchpad.net/~belkacem77'),
  ]),
  ('Malay', 'ms', True, [
    ('abuyop', 'https://launchpad.net/~abuyop'),
  ]),
  ('Polish', 'pl', False, [
    ('Szymon Nieznański', 'https://launchpad.net/~isamu715'),
    ('Michał Rzepiński', 'https://launchpad.net/~micou8'),
  ]),
  ('Brazilian Portuguese', 'pt_BR', False, [
    ('Fábio Nogueira', 'https://launchpad.net/~fnogueira'),
    ('Rodrigo Borges Freitas', 'https://launchpad.net/~rodrigo-borges-freitas'),
    ('Rafael Neri', 'https://launchpad.net/~rafepel'),
  ]),
  ('Russian', 'ru', False, [
    ('Aleksey Kabanov', 'https://launchpad.net/~ak099'),
    ('☠Jay ZDLin☠', 'https://launchpad.net/~black-buddha666'),
    ('Oleg Koptev', 'https://launchpad.net/~koptev-oleg'),
    ('scientistnik', 'https://launchpad.net/~nozdrin-plotnitsky'),
  ]),
  ('Ukrainian', 'uk', True, [
    ('Yuri Chornoivan', 'https://launchpad.net/~yurchor-gmail'),
    ('yurchor', 'https://launchpad.net/~yurchor-deactivatedaccount'),
  ]),
  ('Uzbek', 'uz', False, [
    ('Akmal Xushvaqov', 'https://launchpad.net/~uzadmin'),
  ]),
  ('Chinese (Traditional)', 'zh_TW', False, [
    ('Po-Chun Huang', 'https://launchpad.net/~aphroteus'),
  ]),
]


# generated from pybiklib/ext/gl_ogl.pxd

from libc.stdio cimport printf, puts
cimport gl_[[GLVARIANT]] as gl

print('Importing module:', __name__)
print('  GL-type: [[GLVARIANT]]')

def set_debug_flags(module):
    pass



cdef void _gl_check_error() nogil:
    cdef gl.GLenum err
    err = gl.glGetError()
    while err:
        if err == gl.GL_INVALID_ENUM:
            puts('glerror: GL_INVALID_ENUM')
        elif err == gl.GL_INVALID_VALUE:
            puts('glerror: GL_INVALID_VALUE')
        elif err == gl.GL_INVALID_OPERATION:
            puts('glerror: GL_INVALID_OPERATION')
        elif err == gl.GL_OUT_OF_MEMORY:
            puts('glerror: GL_OUT_OF_MEMORY')
        else:
            printf('gl unknown: %x\n', err)
        err = gl.glGetError()


# functions from /usr/include/GL/gl.h:

cdef void  glClearColor( GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha ) nogil:
    puts('''gl:  void  glClearColor( GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha )''')
    gl. glClearColor(red, green, blue, alpha)
    _gl_check_error()
cdef void  glClear( GLbitfield mask ) nogil:
    puts('''gl:  void  glClear( GLbitfield mask )''')
    gl. glClear(mask)
    _gl_check_error()
cdef void  glCullFace( GLenum mode ) nogil:
    puts('''gl:  void  glCullFace( GLenum mode )''')
    gl. glCullFace(mode)
    _gl_check_error()
cdef void  glFrontFace( GLenum mode ) nogil:
    puts('''gl:  void  glFrontFace( GLenum mode )''')
    gl. glFrontFace(mode)
    _gl_check_error()
cdef void  glEnable( GLenum cap ) nogil:
    puts('''gl:  void  glEnable( GLenum cap )''')
    gl. glEnable(cap)
    _gl_check_error()
cdef void  glDisable( GLenum cap ) nogil:
    puts('''gl:  void  glDisable( GLenum cap )''')
    gl. glDisable(cap)
    _gl_check_error()
cdef GLboolean  glIsEnabled( GLenum cap ) nogil:
    puts('''gl:  GLboolean  glIsEnabled( GLenum cap )''')
    cdef GLboolean result = gl. glIsEnabled(cap)
    _gl_check_error()
    return result
cdef void  glGetBooleanv( GLenum pname, GLboolean *params ) nogil:
    puts('''gl:  void  glGetBooleanv( GLenum pname, GLboolean *params )''')
    gl. glGetBooleanv(pname, params)
    _gl_check_error()
cdef void  glGetFloatv( GLenum pname, GLfloat *params ) nogil:
    puts('''gl:  void  glGetFloatv( GLenum pname, GLfloat *params )''')
    gl. glGetFloatv(pname, params)
    _gl_check_error()
cdef void  glGetIntegerv( GLenum pname, GLint *params ) nogil:
    puts('''gl:  void  glGetIntegerv( GLenum pname, GLint *params )''')
    gl. glGetIntegerv(pname, params)
    _gl_check_error()
cdef GLenum  glGetError() nogil:
    puts('''gl:  GLenum  glGetError( void )''')
    return gl. glGetError()
cdef GLubyte *  glGetString( GLenum name ) nogil:
    puts('''gl:  GLubyte *  glGetString( GLenum name )''')
    cdef GLubyte * result = <GLubyte *>gl. glGetString(name)
    _gl_check_error()
    return result
cdef void  glViewport( GLint x, GLint y,
                                    GLsizei width, GLsizei height ) nogil:
    puts('''gl:  void  glViewport( GLint x, GLint y,
                                    GLsizei width, GLsizei height )''')
    gl. glViewport(x, y, width, height)
    _gl_check_error()
cdef void  glDrawArrays( GLenum mode, GLint first, GLsizei count ) nogil:
    puts('''gl:  void  glDrawArrays( GLenum mode, GLint first, GLsizei count )''')
    gl. glDrawArrays(mode, first, count)
    _gl_check_error()
cdef void  glReadPixels( GLint x, GLint y,
                                    GLsizei width, GLsizei height,
                                    GLenum format, GLenum type,
                                    GLvoid *pixels ) nogil:
    puts('''gl:  void  glReadPixels( GLint x, GLint y,
                                    GLsizei width, GLsizei height,
                                    GLenum format, GLenum type,
                                    GLvoid *pixels )''')
    gl. glReadPixels(x, y, width, height, format, type, pixels)
    _gl_check_error()
cdef void  glTexImage2D( GLenum target, GLint level,
                                    GLint internalFormat,
                                    GLsizei width, GLsizei height,
                                    GLint border, GLenum format, GLenum type,
                                    GLvoid *pixels ) nogil:
    puts('''gl:  void  glTexImage2D( GLenum target, GLint level,
                                    GLint internalFormat,
                                    GLsizei width, GLsizei height,
                                    GLint border, GLenum format, GLenum type,
                                    GLvoid *pixels )''')
    gl. glTexImage2D(target, level, internalFormat, width, height, border, format, type, pixels)
    _gl_check_error()
cdef void  glTexSubImage2D( GLenum target, GLint level,
                                       GLint xoffset, GLint yoffset,
                                       GLsizei width, GLsizei height,
                                       GLenum format, GLenum type,
                                       GLvoid *pixels ) nogil:
    puts('''gl:  void  glTexSubImage2D( GLenum target, GLint level,
                                       GLint xoffset, GLint yoffset,
                                       GLsizei width, GLsizei height,
                                       GLenum format, GLenum type,
                                       GLvoid *pixels )''')
    gl. glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels)
    _gl_check_error()
cdef void  glActiveTexture( GLenum texture ) nogil:
    puts('''gl:  void  glActiveTexture( GLenum texture )''')
    gl. glActiveTexture(texture)
    _gl_check_error()


# functions from /usr/include/GL/glext.h:

cdef void  glBindBuffer (GLenum target, GLuint buffer) nogil:
    puts('''gl:  void  glBindBuffer (GLenum target, GLuint buffer)''')
    gl. glBindBuffer (target, buffer)
    _gl_check_error()
cdef void  glDeleteBuffers (GLsizei n, GLuint *buffers) nogil:
    puts('''gl:  void  glDeleteBuffers (GLsizei n, GLuint *buffers)''')
    gl. glDeleteBuffers (n, buffers)
    _gl_check_error()
cdef void  glGenBuffers (GLsizei n, GLuint *buffers) nogil:
    puts('''gl:  void  glGenBuffers (GLsizei n, GLuint *buffers)''')
    gl. glGenBuffers (n, buffers)
    _gl_check_error()
cdef void  glBufferData (GLenum target, GLsizeiptr size, void *data, GLenum usage) nogil:
    puts('''gl:  void  glBufferData (GLenum target, GLsizeiptr size, void *data, GLenum usage)''')
    gl. glBufferData (target, size, data, usage)
    _gl_check_error()
cdef void  glBufferSubData (GLenum target, GLintptr offset, GLsizeiptr size, void *data) nogil:
    puts('''gl:  void  glBufferSubData (GLenum target, GLintptr offset, GLsizeiptr size, void *data)''')
    gl. glBufferSubData (target, offset, size, data)
    _gl_check_error()
cdef void  glAttachShader (GLuint program, GLuint shader) nogil:
    puts('''gl:  void  glAttachShader (GLuint program, GLuint shader)''')
    gl. glAttachShader (program, shader)
    _gl_check_error()
cdef void  glBindAttribLocation (GLuint program, GLuint index, GLchar *name) nogil:
    puts('''gl:  void  glBindAttribLocation (GLuint program, GLuint index, GLchar *name)''')
    gl. glBindAttribLocation (program, index, name)
    _gl_check_error()
cdef void  glCompileShader (GLuint shader) nogil:
    puts('''gl:  void  glCompileShader (GLuint shader)''')
    gl. glCompileShader (shader)
    _gl_check_error()
cdef GLuint  glCreateProgram () nogil:
    puts('''gl:  GLuint  glCreateProgram (void)''')
    cdef GLuint result = gl. glCreateProgram ()
    _gl_check_error()
    return result
cdef GLuint  glCreateShader (GLenum type) nogil:
    puts('''gl:  GLuint  glCreateShader (GLenum type)''')
    cdef GLuint result = gl. glCreateShader (type)
    _gl_check_error()
    return result
cdef void  glDeleteProgram (GLuint program) nogil:
    puts('''gl:  void  glDeleteProgram (GLuint program)''')
    gl. glDeleteProgram (program)
    _gl_check_error()
cdef void  glDeleteShader (GLuint shader) nogil:
    puts('''gl:  void  glDeleteShader (GLuint shader)''')
    gl. glDeleteShader (shader)
    _gl_check_error()
cdef void  glDetachShader (GLuint program, GLuint shader) nogil:
    puts('''gl:  void  glDetachShader (GLuint program, GLuint shader)''')
    gl. glDetachShader (program, shader)
    _gl_check_error()
cdef void  glDisableVertexAttribArray (GLuint index) nogil:
    puts('''gl:  void  glDisableVertexAttribArray (GLuint index)''')
    gl. glDisableVertexAttribArray (index)
    _gl_check_error()
cdef void  glEnableVertexAttribArray (GLuint index) nogil:
    puts('''gl:  void  glEnableVertexAttribArray (GLuint index)''')
    gl. glEnableVertexAttribArray (index)
    _gl_check_error()
cdef void  glGetActiveAttrib (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) nogil:
    puts('''gl:  void  glGetActiveAttrib (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name)''')
    gl. glGetActiveAttrib (program, index, bufSize, length, size, type, name)
    _gl_check_error()
cdef void  glGetActiveUniform (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) nogil:
    puts('''gl:  void  glGetActiveUniform (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name)''')
    gl. glGetActiveUniform (program, index, bufSize, length, size, type, name)
    _gl_check_error()
cdef GLint  glGetAttribLocation (GLuint program, GLchar *name) nogil:
    puts('''gl:  GLint  glGetAttribLocation (GLuint program, GLchar *name)''')
    cdef GLint result = gl. glGetAttribLocation (program, name)
    _gl_check_error()
    return result
cdef void  glGetProgramiv (GLuint program, GLenum pname, GLint *params) nogil:
    puts('''gl:  void  glGetProgramiv (GLuint program, GLenum pname, GLint *params)''')
    gl. glGetProgramiv (program, pname, params)
    _gl_check_error()
cdef void  glGetProgramInfoLog (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog) nogil:
    puts('''gl:  void  glGetProgramInfoLog (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog)''')
    gl. glGetProgramInfoLog (program, bufSize, length, infoLog)
    _gl_check_error()
cdef void  glGetShaderiv (GLuint shader, GLenum pname, GLint *params) nogil:
    puts('''gl:  void  glGetShaderiv (GLuint shader, GLenum pname, GLint *params)''')
    gl. glGetShaderiv (shader, pname, params)
    _gl_check_error()
cdef void  glGetShaderInfoLog (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog) nogil:
    puts('''gl:  void  glGetShaderInfoLog (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog)''')
    gl. glGetShaderInfoLog (shader, bufSize, length, infoLog)
    _gl_check_error()
cdef GLint  glGetUniformLocation (GLuint program, GLchar *name) nogil:
    puts('''gl:  GLint  glGetUniformLocation (GLuint program, GLchar *name)''')
    cdef GLint result = gl. glGetUniformLocation (program, name)
    _gl_check_error()
    return result
cdef void  glLinkProgram (GLuint program) nogil:
    puts('''gl:  void  glLinkProgram (GLuint program)''')
    gl. glLinkProgram (program)
    _gl_check_error()
cdef void  glShaderSource (GLuint shader, GLsizei count, GLchar **string, GLint *length) nogil:
    puts('''gl:  void  glShaderSource (GLuint shader, GLsizei count, GLchar **string, GLint *length)''')
    gl. glShaderSource (shader, count, <const GLchar *const*>string, length)
    _gl_check_error()
cdef void  glUseProgram (GLuint program) nogil:
    puts('''gl:  void  glUseProgram (GLuint program)''')
    gl. glUseProgram (program)
    _gl_check_error()
cdef void  glUniform1i (GLint location, GLint v0) nogil:
    puts('''gl:  void  glUniform1i (GLint location, GLint v0)''')
    gl. glUniform1i (location, v0)
    _gl_check_error()
cdef void  glUniformMatrix4fv (GLint location, GLsizei count, GLboolean transpose, GLfloat *value) nogil:
    puts('''gl:  void  glUniformMatrix4fv (GLint location, GLsizei count, GLboolean transpose, GLfloat *value)''')
    gl. glUniformMatrix4fv (location, count, transpose, value)
    _gl_check_error()
cdef void  glValidateProgram (GLuint program) nogil:
    puts('''gl:  void  glValidateProgram (GLuint program)''')
    gl. glValidateProgram (program)
    _gl_check_error()
cdef void  glVertexAttribPointer (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, void *pointer) nogil:
    puts('''gl:  void  glVertexAttribPointer (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, void *pointer)''')
    gl. glVertexAttribPointer (index, size, type, normalized, stride, pointer)
    _gl_check_error()
